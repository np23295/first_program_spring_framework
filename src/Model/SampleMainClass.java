package Model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SampleMainClass {
	public static void main(String[] args){
		try{
		System.out.println("Arguement : "+ args);
		}catch(Exception e){
			e.printStackTrace();
		}
		 ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml"); //Getting Application Context
		 SampleModel model=(SampleModel)context.getBean("SpringSampleProgram");   //Getting Bean with Id from XML file specified in Context.
		 model.getText();  //Getting Text From Bean
	}
}
